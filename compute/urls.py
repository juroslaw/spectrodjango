from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static

app_name = 'compute'

urlpatterns = [
    url(r'^$',views.elements,name='elements'),
    url(r"potentials/.*/channels$", views.channels, name='channels'),
    url(r"potentials/(?P<mass1>\d+\.\d+)/(?P<symbol1>\w+)/(?P<mass2>\d+\.\d+)/(?P<symbol2>\w+)/$", views.potentials, name='potentials'),
    url(r'files/$',views.view_files, name='files'),
    url(r"files/delete-(?P<id>\d)$", views.delete_file, name='delete_file'),
    url(r"files/(?P<id>\d)/$", views.file_detail, name='file_detail'),
    url(r'results/$',views.results, name='results'),
    url(r'results/delete-(?P<id>\d*)$',views.delete_result, name='delete_result'),
    url(r'results/download-(?P<id>\d*)$',views.download_result, name='download_result'),
    url(r'results/(?P<result_id>\d*)/$',views.result_detail, name='result_detail'),
    url(r'j_result-(?P<j_result_id>\d*)/$',views.j_result_detail, name='j_result_detail'),
    url('uploadfile', views.FileFieldView.as_view(), name='upload_file')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
