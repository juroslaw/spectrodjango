# Generated by Django 2.1.2 on 2018-11-14 16:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('compute', '0003_auto_20181113_1947'),
    ]

    operations = [
        migrations.AddField(
            model_name='userfile',
            name='delimiter',
            field=models.CharField(choices=[('\t', 'tab'), (',', 'comma'), (';', 'semi-colon'), ('^', 'caret'), ('|', 'pipe')], default='\t', max_length=2),
        ),
    ]
