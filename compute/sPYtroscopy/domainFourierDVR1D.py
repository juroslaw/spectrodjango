import logging
import numpy as np
import scipy
import scipy.linalg
from scipy import interpolate
from scipy.interpolate import CubicSpline
import datetime

class Domain_Fourier_DVR_1D(object):
    """
    The code is based on the examples provided by Jiri Eliasek and Ondrej Marsalek in the github repository
    https://github.com/OndrejMarsalek/Fourier-DVR-1D
    
    Solves the Schroedinger equation on a finite one-dimensional interval using
    the discrete variable representation method with the Fourier sine basis:

    phi_k = sqrt(2 / (b-a)) * sin(k * pi * (x-a) / (b-a)), k=1..n_DVR

    For details, see for example Appendix 2 of:
    J. Chem. Phys. 104, 8433 (1996)
    http://dx.doi.org/10.1063/1.471593
    """

    def __init__(self, a, b, n_DVR, n_ch, type_of_pot, type_of_per):
        """Constructs the domain with given end points and basis size.

        :param a: lower bound of domain
        :param b: upper bound of domain
        :param n_DVR: number of basis functions
        :param n_ch: number of channels
        :param type_of_pot: representation of the potential: analytic or pointwise
        :param type_of_per: representation of the perturbation: analytic or pointwise

        """

        # store domain parameters
        self._a = a
        self._b = b
        self._n_DVR = n_DVR
        self._n_ch = n_ch
        self._type_of_pot = type_of_pot
        self._type_of_per = type_of_per

        # no previous calculation was performed
        self._m = None
        self._m_ref = None
        self._n_states = None
        self._J = None
        self._V = None
        self._V_qn = None
        self._P = None

        # update spectral decomposition of the position operator
        self._update_X_in_Fourier_basis()

    def _update_X_in_Fourier_basis(self):
        """Decompose the position operator in the Fourier sine basis.

        Eigenvalues and eigenvectors of the position operator
        in the Fourier sine basis are stored in `self`.

        """

        logging.debug('X decomposition | start')

        a = self._a
        b = self._b
        n_DVR = self._n_DVR
        n_ch = self._n_ch

        # construct position operator in Fourier sine basis
        i = np.arange(1, n_DVR+1, dtype=float)[:, np.newaxis].repeat(n_DVR, axis=1)
        j = i.transpose()
        ipj = i + j
        ipjmod1 = (ipj) % 2
        div = np.where(ipjmod1,  ((i-j) * ipj)**2, 1)
        fact = -8.0 * (b-a) / np.pi**2
        X_four = fact * np.where(ipjmod1, (i*j) / div, 0.0)

        x, phi_four = scipy.linalg.eigh(X_four)
        
        n_phi_four = [phi_four] * n_ch
        
        #print(n_phi_four)
        phi_four = scipy.linalg.block_diag(*n_phi_four)
        
        x = x + (a+b) / 2.0
        
        self._x = x
        self._phi_four = phi_four
        
        # the shape of X matrix and the grid points x - just for tests 
        #print("x")
        #print(self._x)
        #print("phi_four")
        #print(self._phi_four)
    
        logging.debug('X decomposition | done')

    def _update_T_four(self, m):
        """Build the kinetic energy operator and store it in `self`.

        :param m: mass

        """

        n_ch = self._n_ch
        #phi_four = self._phi_four
        
        if self._m is None or (m != self._m):

            self._m = m

            l = self._b - self._a
            t = (0.5 / m) * (np.pi / l)**2 * np.arange(1, self._n_DVR + 1)**2
            t = np.tile(t, n_ch)
            
            
            #T = np.diagflat(t)
            #self._T_four = np.dot(phi_four.transpose(), np.dot(T, phi_four))
            #self._T_four = np.dot(np.dot(phi_four, T), phi_four.transpose())
            
            self._T_four = np.diagflat(t)
            
            #the shape of the kinetic energy operator - just for tests
            #print("T_four")
            #print(self._T_four)

            logging.debug('build T | done')

    
    def _update_P(self, P):
        """"Build the perturbation matrix and store it in `self`.
        
        :param P: perturbation matrix - real-space vectorized function
        
        """
        
        n_ch = self._n_ch
        n_DVR = self._n_DVR
        type_of_per = self._type_of_per
        
        #or self._J is None or J != self._J
        if np.any(self._P is None or P != self._P):
            self._P = P
            
            if type_of_per == 'analytic':
                #print("analytic perturbation")

                s_tmp = (n_DVR, n_DVR)
                zero_matrix = np.zeros(s_tmp)
        
                rowP = [[] for i in range(n_ch)]

                for iii in range(0, n_ch):
                    rowP[iii] = zero_matrix
                    for kkk in range(0, n_ch-1):
                        if kkk < iii:
                            rowP[iii] = np.hstack((rowP[iii], zero_matrix))
                        else:
                            if iii == 0:
                                rowP[iii] = np.hstack((rowP[iii], np.diagflat(P[kkk](self._x))))
                            else:
                                rowP[iii] = np.hstack((rowP[iii], np.diagflat(P[kkk+iii](self._x))))

                mat = rowP[0]
                for qqq in range(1, n_ch):
                    mat = np.vstack((mat, rowP[qqq]))

                self._P = mat + np.transpose(mat)
            
            else:
                #print("spline perturbation")
                
                xper_to_interpolation = [[] for i in range(n_ch)]
                yper_to_interpolation = [[] for i in range(n_ch)]
                
                ncs_per = [[] for i in range(n_ch)]
                #per = [[] for i in range(n_ch)]
                
                s_tmp = (n_DVR, n_DVR)
                zero_matrix = np.zeros(s_tmp)
            
                #rowP = [[] for i in range(n_ch)]
                
                #print(P)
                #print(len(P))
            
                for i_num_per in range(len(P)):
                    #kolejność punktów jest ważna dla interpolacji - od małych odległości do dużych
                    #print(i_num_per)
                    if (P[i_num_per][0,0] - P[i_num_per][1,0]) > 0:
                        P[i_num_per] = np.flip(P[i_num_per], 0)
                        
                    #P[i_num_per] po ograniczeniu z lewej
                    P[i_num_per] = P[i_num_per][P[i_num_per][:,0]>=self._a]
                    
                    #P[i_num_per] po ograniczeniu z prawej
                    P[i_num_per] = P[i_num_per][P[i_num_per][:,0]<=self._b]
                    
                    # oryginalne punkty zaburzenia, na których będzie robiona interpolacja
                    xper_to_interpolation[i_num_per] = P[i_num_per][:,0]
                    yper_to_interpolation[i_num_per] = P[i_num_per][:,1]
                    
                    # obliczenie punktów interpolacji
                    #ncs - Cubic spline data interpolator with bc_type='natural' - The second derivative at curve ends are zero.
                    ncs_per[i_num_per] = CubicSpline(xper_to_interpolation[i_num_per], yper_to_interpolation[i_num_per],
                                                     bc_type='natural', extrapolate=None)
                    
                    row_up = np.hstack((zero_matrix, np.diagflat(ncs_per[0](self._x))))
                    row_down = np.hstack((np.diagflat(ncs_per[0](self._x)), zero_matrix))
                    
                    full_P = np.vstack((row_up, row_down))
            
                self._P = full_P
                
                #print(P)
                #print(xper_to_interpolation)
                #print(yper_to_interpolation)
                #print(len(ncs_per[0](self._x)))
                #print(zero_matrix.shape)
                #print(row_up)
                #print("---")
                #print(row_down)
                #print("---")
                #print(full_P)
            
            #old code for two channels
            #if n_ch == 2:
            #    s_tmp = (n_DVR, n_DVR)
            #    zero_matrix = np.zeros(s_tmp)
            #    
            #    P_LP = np.hstack((zero_matrix, np.diagflat(P[0](self._x))))
            #    P_PL = np.hstack((np.diagflat(P[0](self._x)), zero_matrix))
            #    self._P = np.vstack((P_LP, P_PL))
            #else:
            #    s_tmp = (n_DVR*n_ch, n_DVR*n_ch)
            #    zero_matrix = np.zeros(s_tmp)
            #    self._P = zero_matrix
            
            #if(n_ch == 1):
            #    self._phi_four = phi_four
            #else:
            #P_pr = zero_matrix
            #for ii in range(len(P)):
            #    print("aaa")
            #    P_pr = np.hstack((P_pr, np.diagflat(P[ii](self._x))))
            #
            #
            #    phi_four_bis = phi_four_pr
            #    for ii in range(n_ch-1):
            #        print("bbb")
            #        phi_four_bis = np.vstack((phi_four_bis, phi_four_pr))
            #    
            #    self._phi_four = phi_four_bis
            #
            #one_matrix = np.diagflat(np.ones(n_DVR))
            #
            #vert_P_LZ_RO = np.hstack((zero_matrix, one_matrix))
            #vert_P_LO_RZ = np.hstack((one_matrix, zero_matrix))
            #
            #P_matrix = np.vstack((vert_P_LZ_RO, vert_P_LO_RZ)) 
            
            
            #the shape of the perturbation - just for tests
            #print("P matrix")
            #print(self._P)
        
            logging.debug('build P | done')
    
    
    def _update_V_four(self, V, V_qn, m, m_ref, J):
        """Build the potential energy operator and store it in `self`.

        :param V: potential energy - real-space vectorized function

        """

        #TODO lambda doubling for all potential cases: analytic, bspline
        
        n_ch = self._n_ch
        n_DVR = self._n_DVR
        type_of_pot = self._type_of_pot
        #m = self._m
        phi_four = self._phi_four
        
        
        if self._V is None or (V != self._V) or self._J is None or (J != self._J):
            self._V = V
            
            if type_of_pot == 'analytic':
                # when potential is given in the analytic form
                print("analytic potential")
                #remember about Lambda in the rotational part
                #Lambda = 0

                for i_num_pot in range(0, n_ch):
                    if i_num_pot == 0:
                        U = V[0](self._x) + (1/(2*m*self._x**2))*(J*(J+1)-V_qn[i_num_pot][0]**2)
                        #print("test _U_update ii=0")
                        #print(U)
                    else:
                        U = np.append(U, V[i_num_pot](self._x) + (1/(2*m*self._x**2))*(J*(J+1)-V_qn[i_num_pot][0]**2))
                        #print("test _U_update ii nie 0")
                        #print(U)
            elif type_of_pot == 'bspline':
                # bsplines - when potential is given in the pointwise form
                print("B-splines")
                x_to_interpolation = [[] for i in range(n_ch)]
                y_to_interpolation = [[] for i in range(n_ch)]
                tck = [[] for i in range(n_ch)]
                U = [[] for i in range(n_ch)]
                
                #remember about Lambda in the rotational part
                #Lambda = 1
                
                #TODO - oprogramować wiele kanałów i zrobić diagnostykę
                
                for i_num_pot in range(n_ch):
                    #kolejność punktów jest ważna dla interpolacji - od małych odległości do dużych
                    if (V[i_num_pot][0,0] - V[i_num_pot][1,0]) > 0:
                        V[i_num_pot] = np.flip(V[i_num_pot], 0)
                        
                    # V[i_num_pot] po ograniczeniu z lewej
                    V[i_num_pot] = V[i_num_pot][V[i_num_pot][:,0]>=self._a]
                    
                    # V[i_num_pot] po ograniczeniu z prawej
                    V[i_num_pot] = V[i_num_pot][V[i_num_pot][:,0]<=self._b]
                
                    # oryginalne punkty potencjałów, na których będzie robiona interpolacja
                    x_to_interpolation[i_num_pot] = V[i_num_pot][:,0]
                    y_to_interpolation[i_num_pot] = V[i_num_pot][:,1]
                    
                    # obliczenie parametrów interpolacji
                    #(t,c,k) a tuple containing the vector of knots, the B-spline coefficients, and the degree of the spline
                    tck[i_num_pot] = interpolate.splrep(x_to_interpolation[i_num_pot], y_to_interpolation[i_num_pot], s=0)
                    
                    # interpolacja potencjałów w punktach DVR - self._x i dodanie części związanej z rotacjami
                    U[i_num_pot] = interpolate.splev(self._x, tck[i_num_pot], der=0) + (1/(2*m*self._x**2))*(J*(J+1)-V_qn[i_num_pot][0]**2)
            else:
                # natural cubic splines - when potential is given in the pointwise form
                #print("natural cubic splines")
                x_to_interpolation = [[] for i in range(n_ch)]
                y_to_interpolation = [[] for i in range(n_ch)]
                
                ncs = [[] for i in range(n_ch)]
                U = [[] for i in range(n_ch)]
                
                for i_num_pot in range(n_ch):
                    #kolejność punktów jest ważna dla interpolacji - od małych odległości do dużych
                    if (V[i_num_pot][0,0] - V[i_num_pot][1,0]) > 0:
                        V[i_num_pot] = np.flip(V[i_num_pot], 0)
                        
                    # V[i_num_pot] po ograniczeniu z lewej
                    V[i_num_pot] = V[i_num_pot][V[i_num_pot][:,0]>=self._a]
                    
                    # V[i_num_pot] po ograniczeniu z prawej
                    V[i_num_pot] = V[i_num_pot][V[i_num_pot][:,0]<=self._b]
                
                    # oryginalne punkty potencjałów, na których będzie robiona interpolacja
                    x_to_interpolation[i_num_pot] = V[i_num_pot][:,0]
                    y_to_interpolation[i_num_pot] = V[i_num_pot][:,1]
                    
                    # obliczenie parametrów interpolacji
                    #ncs - Cubic spline data interpolator with bc_type='natural' - The second derivative at curve ends are zero.
                    ncs[i_num_pot] = CubicSpline(x_to_interpolation[i_num_pot], y_to_interpolation[i_num_pot],
                                                 bc_type='natural', extrapolate=None)
                    
                    # interpolacja potencjałów w punktach DVR - self._x i dodanie części związanej z rotacjami
                    # dodanie lambda doubling dla lambda > 0 i stanów e 0.0000695
                    if (V_qn[i_num_pot][0]>0) and (V_qn[i_num_pot][1]=='e'):
                        U[i_num_pot] = ncs[i_num_pot](self._x) + (1/(2*m*self._x**2))*(J*(J+1) - V_qn[i_num_pot][0]**2 + J*(J+1)*V_qn[i_num_pot][2]*(m_ref/m))
                    else:
                        U[i_num_pot] = ncs[i_num_pot](self._x) + (1/(2*m*self._x**2))*(J*(J+1) - V_qn[i_num_pot][0]**2)
            
            
            #for Pi -> -2.27817e-05
            #for Sigma -> +2.27817e-05
            
            # tworzenie macierzy z potencjałem na diagonali (bez zaburzeń)
            # lub z potencjałem na diagonali i z elementami pozadiagonalnymi w przypadku wprowadzenia zaburzenia
            if self._P is None or (n_ch == 1):
                V_x = np.diagflat(U)
            else:
                V_x = np.diagflat(U) + self._P
            
            #this is sometimes needed for tests
            #print("size of V_x")
            #print(np.shape(phi_four))
            #print(np.shape(V_x))
            #print("V_x")
            #print(V_x)
            
            self._V_four = np.dot(np.dot(phi_four, V_x), phi_four.transpose())
            
            #the shape of potential energy operator with  - just for tests
            #print("V_four")
            #print(self._V_four)

            logging.debug('build V | done')

    def solve(self, m, m_ref, V, V_qn, J, P=None, n_states=None, calc_eigenstates=True):
        """Solve the Schroedinger equation on this domain.

        :param m: mass
        :const m_ref: reduced mass of the reference isotopologue, which is the most abundant isotopologue (needed for lambda doubling)
        :param V: potential energy - real-space vectorized function
        :param V_qn: quantum numbers describing each channel
        :param J: rotational quantum number
        :param P: perturbation matrix - real-space vectorized function
        :param n_states: number of states to calculate
        :param calc_eigenstates: whether to return eigenstates as well

        Returns eigenenergies and (optionally) eigenstates of the Hamiltonian
        sorted by eigenenergy magnitude.

        """

        logging.debug('solve | start')
        
        self._n_states = n_states

        #if J is None:
        #    self._J = 0
        #else:
        #    self._J = J                    
        
        if n_states is None:
            eigvals = None
        else:
            eigvals = (0, n_states)

        # update kinetic energy and potential operators, as well as perturbation matrix if needed
        self._update_T_four(m)
        
        if P is None or (self._n_ch) == 1:
            self._update_V_four(V, V_qn, m, m_ref, J)
        else:
            self._update_P(P)
            self._update_V_four(V, V_qn, m, m_ref, J)

        # construct the Hamiltonian
        H_four = self._T_four + self._V_four
        
        #the shape of the total hamiltonian - just for tests
        #print("H_four")
        #print(H_four)
        
        logging.debug('solve | Hamiltonian built')

        # solve
        eigvals_only = not calc_eigenstates
        
        #t1=datetime.datetime.now()
        result = scipy.linalg.eigh(H_four,
                                   eigvals=eigvals,
                                   eigvals_only=eigvals_only,
                                   overwrite_a=True)
        
        #t2=datetime.datetime.now()
        #print(t2-t1)
        #Eigenvalues and eigenvector in arrays - just for tests
        #print("Eigenvalues and eigenvector")
        #print(result)

        logging.debug('solve | done')
        
        return result


    def coefficients(self, eigenvectors):
        """"Evaluate coefficients of each channel psi = a_i phi_i + b_i phi_i + ...
        a = sum(i) (a_i)^2 - channel 1
        b = sum(i) (b_i)^2 - channel 2
        ...
        
        :pram eigenvectors: states in Fourier basis (E_four)
        
        Returns coefficients of each channel.
        
        """
        
        logging.debug('coefficients | start')
        
        n_ch = self._n_ch
        n_DVR = self._n_DVR
        n_states = self._n_states
        
        #vec_coef_shape = (n_DVR*n_ch, n_ch)
        vec_coef_shape = (n_states+1, n_ch)
        
        vec_of_cols = np.hsplit(eigenvectors, eigenvectors.shape[1])
        vec_coef = np.zeros(vec_coef_shape)
        
        for ii in range(0, len(vec_of_cols)):
            coef = np.square(np.hsplit(vec_of_cols[ii].transpose(), n_ch))
            for jj in range(0, len(coef)):
                vec_coef[ii, jj] += np.sum(coef[jj])
        
        #array of vector of coefficient - just for tests
        #print("Vector of coefficients")
        #print(vec_coef)
        
        logging.debug('coefficients | done')
        
        return vec_coef
        
    
    def grid(self, x, psi_four):
        """Evaluate states on a real-space grid.

        :param x: real-space grid - 1D array
        :param psi_four: states in Fourier basis

        Returns states on grid x.

        """

        logging.debug('grid | start')

        n_DVR, n_out = psi_four.shape
        
        #print psi_four - just for tests
        #print("psi_four")
        #print(psi_four)
        
        #sometimes needed to check dimensions
        #print("n_DVR")
        #print(n_DVR)
        #print("n_out")
        #print(n_out)
        #print("self._n_DVR")
        #print(self._n_DVR)
        #print("self._n_ch")
        #print(self._n_ch)

        if n_DVR != self._n_DVR*self._n_ch:
            data = (self._n_DVR, n_DVR)
            err = 'Wrong dimension of states. Expected %d, got %d.' % data
            raise ValueError(err)

        # convenience
        a = self._a
        b = self._b

        # flag points outside the [a, b] interval
        outside = np.logical_and((x > a), (x < b))

        # construct Fourier sine basis functions on real-space grid
        norm = np.sqrt(2 / (b-a))
        four_1_grid = np.exp(1.0j * np.pi * (x-a) / (b-a))
        
        #four_1_grid = np.sin(k * np.pi * (x-a) / (b-a))
        four_1_grid *= outside
        
        k = np.arange(1, self._n_DVR+1, dtype=complex)
        
        if self._n_ch >= 2:
            k1 = k
            for i_in_grid in range(1, self._n_ch):
                k = np.append(k,k1)
#                print(k)
        
        four_grid = norm * np.imag(four_1_grid[:, np.newaxis]**k)
        
        #grid and psi - just for tests
        #print("four_grid before project")
        #print(four_grid.shape)
        #print(four_grid)
        #print("psi_four")
        #print(psi_four)
        
        # project states to real-space grid
        psi_grid = np.dot(four_grid, psi_four).transpose()

        logging.debug('grid | done')

        return psi_grid