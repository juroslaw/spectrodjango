import numpy as np
import pandas as pd
import scipy.constants as const
import os
from collections import deque
from .domainFourierDVR1D import Domain_Fourier_DVR_1D
from compute.models import UserFile
from bokeh.plotting import figure
from bokeh.embed import components
import json
import datetime
import uuid
#from flaskr.modules.plot.utilities import generatePointPotentialPlot, generateAnalyticPotentialPlot


class sPYtroscopy():

    def __init__(self, molecule_string, chosen_channels, reduced_mass, perturbation_values, x_min=6.2, x_max=16.0, v_min=1.0, v_max=1.0, J_min=82, J_max=82, n_DVR=300, n_states=99, n_g=2**14+1, n_ch=2):
        self.x_min = x_min
        self.x_max = x_max
        self.v_min = v_min
        self.v_max = v_max
        self.J_min = J_min
        self.J_max = J_max
        self.n_DVR = n_DVR
        self.n_states = n_states
        self.n_g = n_g
        self.n_ch = n_ch
        self.molecule_string = molecule_string
        self.chosen_channels = chosen_channels
        self.reducedMass = reduced_mass
        self.perturbation_values = perturbation_values
        self.channels = []
        self.V = []
        self.vibEnergies = []
        self.vibCoefficients = []

    def createChannels(self): 
        self.channels = []   
        for channel in self.chosen_channels:
            if channel['data_type'] == 'point':   
                session_str = channel['file'].replace("'", "\"")             
                file_dict = json.loads(session_str)
                data_separator = UserFile.objects.filter(user_file=file_dict['file_selection'])[0].delimiter
                energy_column = file_dict['energy_column']
                energy_unit = file_dict['energy_unit']
                distance_column = file_dict['distance_column']
                distance_unit = file_dict['distance_unit']
                ch = self._loadDataFromFile(file_dict['file_selection'],(distance_column,energy_column), separator=data_separator)                
                self.channels.append(ch)
            elif channel['data_type'] == 'analytical' and channel['analytical_type'] == 'morse':
                D = channel['analytical_D']
                a = channel['analytical_a']
                args = {'D': float(D), 'a': float(a)}
                x = np.linspace(self.x_min, self.x_max, 24)
                potentials = self.morsePotential(x, args)
                dequeToNumpyArrayHelper = deque()
                for i in range(len(x)):
                    dequeToNumpyArrayHelper.append([x[i],convertUnitToAu(potentials[i], 'U_cm')])
                self.channels.append(np.array(dequeToNumpyArrayHelper))
            elif channel['data_type'] == 'analytical' and channel['analytical_type'] == 'lennard-jones':
                D = channel['analytical_D']
                a = channel['analytical_a']
                args = {'D': float(D), 'a': float(a)}
                x = np.linspace(self.x_min, self.x_max, 24)
                potentials = self.lennardJonesPotential(x, args)
                dequeToNumpyArrayHelper = deque()
                for i in range(len(x)):
                    dequeToNumpyArrayHelper.append([x[i],convertUnitToAu(potentials[i], 'U_cm')])
                self.channels.append(np.array(dequeToNumpyArrayHelper))

    def morsePotential(self, x, args):
        D = args['D']
        a = args['a']
        return D * (1 - np.exp(-a*x)) ** 2

    def lennardJonesPotential(self, x, args):
        D = args['D']
        a = args['a']      
        return 4*D * (np.power(a/x,12) - np.power(a/x,6))

    def calculate(self):
        name = f'{self.molecule_string}_' + str(datetime.date.today())
        radial_fun_lunc_param = self.perturbation_values
        results = []
        for J_value in range(self.J_min, self.J_max+1,1):
            type_of_pot = 'ncs'
            self.P = [(lambda x: (-1 / (2 * self.reducedMass * x ** 2)) * (radial_fun_lunc_param[0] * x ** 4 +
                                                                           radial_fun_lunc_param[1] * x ** 3 + 
                                                                           radial_fun_lunc_param[2] * x ** 2 +
                                                                           radial_fun_lunc_param[3] * x ** 1 + 
                                                                           radial_fun_lunc_param[4] * x ** 0) * 
                                                                           (np.sqrt(J_value * (J_value + 1))))]
            V_qn_content = []
            for channel in self.chosen_channels:
                V_qn_content.append([int(channel['lambda_number']), channel['parity'], float(channel['lambda_doubling'])])
            V_qn = V_qn_content
            self.createChannels()
            self.V = self.channels
            domain = Domain_Fourier_DVR_1D(a=self.x_min, b=self.x_max, n_DVR=self.n_DVR, n_ch=self.n_ch , type_of_pot=type_of_pot, type_of_per='analytic') 
            E, E_four = domain.solve(m=self.reducedMass, m_ref=self.reducedMass, V=self.V,
                                    V_qn=V_qn, J=J_value, P=self.P, n_states=self.n_states)
            coeff = domain.coefficients(E_four)
            x = np.linspace(self.x_min, self.x_max, self.n_g)
            psi_x = domain.grid(x, E_four[:,:self.n_states+1])

            current_J_list = [ J_value for i in range(len(E)) ]

            data = {'Energy' : E.tolist(),
                    'Coefficient' : coeff.tolist(),
                    'J': current_J_list}
            downloadableDataFile = pd.DataFrame(data)                       
            fileName = self.molecule_string 
            for channel in self.chosen_channels:
                if channel['data_type'] == 'point':  
                    fileName = fileName + '_' + channel['multi'] + '_' + channel['lambda_number'] 
                else:
                    fileName = fileName + '_' + channel['analytical_type']
            fileName = fileName + '_J' + str(J_value) + '_' + str(datetime.date.today()) + str(uuid.uuid4())[:5] + '.dat'

            downloadFolder = 'uploads/results/'
            file_path = os.path.join(downloadFolder, fileName)
            if not os.path.exists(downloadFolder):
                os.makedirs(downloadFolder)
            with open(file_path, 'w') as temp_file:
                temp_file.write('')

            downloadableDataFile.to_csv(path_or_buf=os.path.join(downloadFolder, fileName), sep='\t')
            
            dataToPlot = []
            for j in range(self.n_ch):
                dataToPlot.append(self.V[j][:,0]) # x for potentials
                dataToPlot.append(self.V[j][:,1]+(1/(2*self.reducedMass*self.V[j][:,0]**2))*(J_value*(J_value+1))) # y for potentials

            interesting_index = 0
            best_value = 1
            for i in range(len(coeff)):
                if abs(coeff[i][0] - coeff[i][1]) < best_value:
                    interesting_index = i
                    best_value =  abs(coeff[i][0] - coeff[i][1])

            scale = 0.001
            if self.P is None:
                perturbationData = None
            else:
                perturbationData = []
                perturbationData.append(x) # x for perturbation
                perturbationData.append((scale * psi_x[interesting_index]) + E[interesting_index]) # y for perturbation
            script, div = generatePointPotentialPlot(diatomicMolecule=self.molecule_string, potentialsNumber=self.n_ch,
                                                    givenData=dataToPlot, perturbationData=perturbationData, J=J_value, quantumState=str(interesting_index))
            results.append((J_value, file_path, (script, div)))
        return results, name

    def _loadDataFromFile(self, fileName, listOfColumns, pathToFile="", separator='\t'):
        if type(listOfColumns) != tuple:
            print("You have to choose two columns: R and F(R)")
        else:
            if len(listOfColumns) == 2:
                if pathToFile == "":
                    pathToFile = os.getcwd()+"\\"
                if os.path.exists(pathToFile+fileName):
                    try:
                        channel_1 = np.loadtxt(pathToFile+fileName, usecols=listOfColumns, delimiter=separator)
                        return channel_1
                    except Exception:
                        print("These columns are not exist in your file.")
                else:
                    print("The file path or file name is not correct. Check also permissions.")
            else:
                print("You have to choose two columns: R and F(R)")


def convertUnit(value, givenUnit, requestedUnit):
    if givenUnit == 'R_A' and requestedUnit == 'R_au':
        return value/(const.physical_constants.get("Bohr radius")[0]/const.angstrom)
    elif givenUnit == 'R_au' and requestedUnit == 'R_A':
        return value*(const.physical_constants.get("Bohr radius")[0]/const.angstrom)
    elif givenUnit == 'U_cm' and requestedUnit == 'U_au':
        return value/(const.physical_constants.get("hartree-inverse meter relationship")[0]/100)
    elif givenUnit == 'U_au' and requestedUnit == 'U_cm':
        return value*(const.physical_constants.get("hartree-inverse meter relationship")[0]/100)
    else:
        return None

def convertUnitToAu(value, givenUnit):
    if givenUnit == 'R_au' or givenUnit == 'U_au':
        return value
    elif givenUnit == 'R_A':
        return convertUnit(value, givenUnit, 'R_au')
    elif givenUnit == 'U_cm':
        return convertUnit(value, givenUnit, 'U_au')
    else:
        return None
        
def generatePointPotentialPlot(diatomicMolecule, potentialsNumber, givenData, perturbationData, J, quantumState):
    DiatomicMoleculeTitle = diatomicMolecule
    tools = "crosshair, pan, wheel_zoom, box_zoom, reset, save"
    uploadedPlot = figure(title=DiatomicMoleculeTitle + '  J=' + str(J) + '; Quantum state=' + quantumState, x_axis_label='Bohr radius', y_axis_label='Hartree energy', tools=tools, plot_width=800, plot_height=500, sizing_mode = 'scale_both')
    uploadedPlot.title.text_font_size = '25pt'
    uploadedPlot.title.align = 'center'
    uploadedPlot.xaxis.axis_label_text_font_size  = '15pt'
    uploadedPlot.yaxis.axis_label_text_font_size  = '15pt'
    
    colorsDatabase = ('black', 'green', 'red', 'pink', 'yellow', 'purple', 'orange')
    lineColors = []

    #load data to the new plot
    j = 0
    for i in range(potentialsNumber):
        lineColors.append(colorsDatabase[j])
        j += 1
        if j >= len(colorsDatabase):
            j = 0
    # potentials
    j = 0
    IDsList = []
    for i in range(1, potentialsNumber+1):
        uploadedPlot.line(givenData[j], givenData[j+1], legend=f"Channel {i}", line_width=2, line_color=lineColors[i-1])
        IDsList.append(i)
        j += 2

    perturbationLegend = 'Channels '
    for ID in IDsList:
        if ID == IDsList[-1]:
            perturbationLegend += str(ID) + ' perturbation'
        else:
            perturbationLegend += str(ID) + ', '
    # perturbation
    if perturbationData is not None:
        uploadedPlot.line(perturbationData[0], perturbationData[1], legend=perturbationLegend, line_width=2, line_color="blue")
    return components(uploadedPlot)