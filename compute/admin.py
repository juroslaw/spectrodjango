from django.contrib import admin
from .models import UserFile

class UserfileAdmin(admin.ModelAdmin):
    list_display = ('user','user_file','timestamp')
admin.site.register(UserFile,UserfileAdmin)