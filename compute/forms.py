from django import forms
from django.forms import formset_factory, BaseFormSet
from .models import UserFile, GlobalParameters, Perturbation, Configuration, Channel


class FileFieldForm(forms.Form):
    delimiter_choices = (
        ('\t', 'tab: \\t'),
        (',','comma: ,'),
        (';','semi-colon: ;'),
        ('^','caret: ^'),
        ('|','pipe: |'),
    )
    file_field = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))
    delimiter_field = forms.ChoiceField(choices=delimiter_choices, required=True)

class PotentialForm(forms.Form):
    def __init__(self, *args, **kwargs):
        extra = kwargs.pop('extra')
        super().__init__(*args, **kwargs)
        self.fields['file_selection'] = forms.ChoiceField(choices=extra)

    distance_unit_choices = (
        ('R_A', 'Angstrom [A]'),
        ('R_au', 'Bohr radius [a_0]')
    )
    energy_unit_choices = (
        ('cm', 'Inverse Centimeter [cm^(-1)]'),
        ('U_au', 'Hartree Energy [E_h]')
    )

    distance_unit = forms.ChoiceField(choices=distance_unit_choices, required=True)
    distance_column = forms.IntegerField(min_value=0, required=True, initial=0)
    energy_unit = forms.ChoiceField(choices=energy_unit_choices, required=True)
    energy_column = forms.IntegerField(min_value=0, required=True, initial=1)

class ChannelForm(forms.Form):
    def __init__(self, *args, **kwargs):
        files = kwargs.pop('files')
        super().__init__(*args, **kwargs)
        if files:
            file_choices = tuple((f,str(f['file_selection']).rsplit("/",1)[-1]) for f in files)     
            self.fields['file'] = forms.ChoiceField(choices=file_choices, required=False)
        # else:

    multiplicity_choices = (
        (1, 'Singlet'),
        (2, 'Doublet'),
        (3, 'Triplet'),
        (4, 'Quartet')        
    )
    lambda_choices = (
        (0, '\u03a3 \u2095'),
        (0, '\u03a3 \u2096'),
        (1, '\u03a0'),
        (2, '\u03b4'),
        (3, '\u03a6')
    )
    parity_choices = (
        ('e', 'e'),
        ('f', 'f')
    )
    omega_choices = (
        ('0+','0+'),
        ('0-','0-'),
        ('1','1'),
        ('2','2'),
        ('3','3'),
    )
    analytical_choices = (
        ('morse', 'Morse'),
        ('lennard-jones', 'Lennard-Jones')
    )
    data_type_choices = (
        ('analytical', 'Analitycal'),
        ('point', 'Point')
    )
    multi = forms.ChoiceField(choices=multiplicity_choices, initial=0)
    lambda_number = forms.ChoiceField(choices=lambda_choices)
    lambda_doubling = forms.DecimalField(initial=0)
    parity = forms.ChoiceField(choices=parity_choices)
    omega = forms.ChoiceField(choices=omega_choices)
    data_type = forms.ChoiceField(choices=data_type_choices, widget=forms.RadioSelect)
    analytical_type = forms.ChoiceField(choices=analytical_choices, required=False)
    analytical_D = forms.DecimalField(initial=0, required=False)
    analytical_a = forms.DecimalField(initial=0, required=False)
            

class GlobalsForm(forms.ModelForm):
    class Meta:
        model = GlobalParameters
        exclude = ['reduced_mass']

class PerturbationForm(forms.ModelForm):
    class Meta:
        model = Perturbation
        exclude = ['configuration']