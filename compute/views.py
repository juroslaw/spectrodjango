from django.shortcuts import render, get_object_or_404, HttpResponse
from django.views.generic.edit import FormView
from django.contrib.auth.models import AnonymousUser
from django.http import HttpResponseRedirect
from .forms import FileFieldForm, PotentialForm, ChannelForm, GlobalsForm, PerturbationForm
from django.forms.formsets import formset_factory
from .models import UserFile, Channel, Configuration, Perturbation, GlobalParameters, Result, J_Result
from compute.sPYtroscopy.sPYtroscopy import sPYtroscopy
import scipy.constants as const
import pandas as pd
import numpy as np
import sys
import csv
import json
import os
import zipfile
import datetime

def results(request):
    if isinstance(request.user, AnonymousUser):
        return HttpResponseRedirect('/accounts/login')
    results = Result.objects.filter(user=request.user)
    return render(request, 'results.html', {'results': results})

def result_detail(request, result_id):
    if isinstance(request.user, AnonymousUser):
        return HttpResponseRedirect('/accounts/login')
    result = get_object_or_404(Result, id=result_id)
    J_results = J_Result.objects.filter(Parent=result)
    return render(request, 'result_detail.html', {'J_results': J_results, 'result_name': result.name})

def j_result_detail(request, j_result_id):
    if isinstance(request.user, AnonymousUser):
        return HttpResponseRedirect('/accounts/login')
    J_result = J_Result.objects.defer(None).filter(id=j_result_id).first()
    plot_script = J_result.plot_script
    Parent = get_object_or_404(Result, id=J_result.Parent.id)
    dataframe = pd.read_csv(J_result.data_file, sep='\t')
    dataframe = dataframe.to_html(classes="table table-control table-striped", columns=['Energy','Coefficient'], sparsify=False)
    return render(request, 'j_result_detail.html', {'J_result': J_result, 'dataframe': dataframe, 'Parent': Parent, 'plot_script': plot_script})

def delete_result(request, id):
    result = get_object_or_404(Result, id=id)
    if result.user != request.user:
        return HttpResponseRedirect('/accounts/logout')
    for j_result in J_Result.objects.filter(Parent=result):
        os.remove(j_result.data_file)
    result.delete()
    return HttpResponseRedirect('./')

def download_result(request,id):
    result = get_object_or_404(Result, id=id)
    j_results = J_Result.objects.filter(Parent=result)
    file_path = 'uploads/tmp/' + result.name + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M") + '.zip'
    zipf = zipfile.ZipFile(file_path, 'w', zipfile.ZIP_DEFLATED)
    for j_result in j_results:
        try: 
            zipf.write(j_result.data_file)
        except Exception as E:
            zipf.close()
            os.remove(file_path)
            return HttpResponseRedirect('../')
    zipf.close()
    zip_file = open(file_path, 'rb')
    response = HttpResponse(zip_file, content_type='application/force-download')
    response['Content-Disposition'] = 'attachment; filename="%s"' % result.name + '.zip'
    zip_file.close()
    os.remove(file_path)
    return response
    

def channels(request, file_config=None, elements_config=None, num_of_channels=2, template_name='channels.html'):
    channels = formset_factory(ChannelForm, extra=2)
    if 'channels' in request.path:
        channel_formset = channels(request.POST, form_kwargs={'files': json.loads(request.session['file_config'])})
    else:
        channel_formset = channels(form_kwargs={'files': file_config})
        request.POST = None
        request.session['file_config'] = json.dumps(file_config)
    global_form = GlobalsForm(request.POST or None, initial={})
    perturbations = PerturbationForm(request.POST or None)
    if channel_formset.is_valid() and global_form.is_valid() and perturbations.is_valid():
        perturbation_values = [ float(perturbations.cleaned_data[value]) for value in perturbations.cleaned_data ]
        chosen_channels = []
        for form in channel_formset:
            chosen_channels.append(form.cleaned_data)
        x_min=global_form.cleaned_data['x_min']
        x_max=global_form.cleaned_data['x_max']
        v_min=global_form.cleaned_data['v_min']
        v_max=global_form.cleaned_data['v_max']
        J_min=global_form.cleaned_data['J_min']
        J_max=global_form.cleaned_data['J_max']
        n_DVR=global_form.cleaned_data['n_DVR']
        n_states=global_form.cleaned_data['n_states']
        elements = request.path.split('/')[3:7]
        elements_config = {
            'mass1': elements[0],
            'symbol1': elements[1], 
            'mass2': elements[2], 
            'symbol2': elements[3]
        }
        reduced_mass = get_reduced_mass(elements_config['mass1'], elements_config['mass2'])
        molecule = elements_config['symbol1'] + elements_config['symbol2']
        sPYtroscopy_instance = sPYtroscopy(molecule_string=molecule, 
                                           chosen_channels=chosen_channels,
                                           reduced_mass=reduced_mass,
                                           perturbation_values=perturbation_values,
                                           x_min=x_min,
                                           x_max=x_max,
                                           v_min=v_min,
                                           v_max=v_max,
                                           J_min=J_min,
                                           J_max=J_max,
                                           n_DVR=n_DVR,
                                           n_states=n_states)
        results = None
        try:
            results, name = sPYtroscopy_instance.calculate()
        except Exception as e:
            return error_view(request, repr(e))
        print(request.user)
        result = Result(user=request.user, name=name, configuration=None)
        result.save()
        for r in results:
            J_result = J_Result(J=r[0], data_file=r[1], plot_div=str(r[2][1]), plot_script=r[2][0], Parent=result)
            J_result.save()
        return HttpResponseRedirect('/compute/results/')
    return render(request, template_name, {'channels': channel_formset,
                                           'elements': elements_config, 
                                           'num_of_channels': range(num_of_channels),
                                           'globals': global_form,
                                           'perturbations': [perturbations],
                                           'channel_matrix': build_channel_matrix(channel_formset,[perturbations])})

def error_view(request, error, template_name='error.html'):
    return render(request, template_name, {'error': error })

def potentials(request, mass1, symbol1, mass2, symbol2, template_name = 'potentials.html'):
    elements = {'mass1': mass1,
                'symbol1': symbol1, 
                'mass2': mass2, 
                'symbol2': symbol2}

    if isinstance(request.user, AnonymousUser):
        return HttpResponseRedirect('/accounts/login')

    choices = []
    for item in UserFile.objects.filter(user=request.user):
        choices.append((item.user_file, str(item.user_file).rsplit("/",1)[-1]))
    PotentialFormset = formset_factory(PotentialForm, min_num=1, max_num=10, validate_max= True, validate_min=True)
    formset = PotentialFormset(request.POST or None, form_kwargs={'extra': choices})
    if formset.is_valid():
        files = []
        for form in formset:
            files.append(form.cleaned_data)
        return channels(request, file_config=files, elements_config=elements, num_of_channels=2)
    return render(request, template_name, {'formset': formset, 'elements': elements } )


def elements(request):
    if isinstance(request.user, AnonymousUser):
        return HttpResponseRedirect('/accounts/login')
    with open('compute/elements.json') as f:
        elements = json.load(f)
        return render(request,'list.html', {'elements': elements} )

def delete_file(request, id):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/accounts/logout')
    userfile = UserFile.objects.filter(id=id).first()
    if userfile.user == request.user:
        userfile.delete()
    return HttpResponseRedirect('/compute/files') 

def view_files(request):
    if isinstance(request.user, AnonymousUser):
        return HttpResponseRedirect('/accounts/login')
    files = UserFile.objects.filter(user=request.user)
    return render(request, 'files.html', {'files': files})

def file_detail(request, id):
    if not request.user.is_authenticated:
        HttpResponseRedirect('/accounts/login')
    userfile = get_object_or_404(UserFile, id=id)
    try:
        file_content= np.loadtxt(userfile.user_file, delimiter=userfile.delimiter)
    except:
        file_content = "Failed to open file"
    return render(request, 'file_detail.html', {'file': userfile,
                                                'file_content': file_content})

class FileFieldView(FormView):
    form_class = FileFieldForm
    template_name = 'upload.html'  
    success_url = '/compute/files' 

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        files = request.FILES.getlist('file_field')
        if form.is_valid():
            delimiter = form.cleaned_data['delimiter_field']
            for f in files:
                if f.size < 2**20:# 1MiB               
                    try:
                        np.loadtxt(f, delimiter=delimiter)
                    except:
                        form.errors['file_error'] =  "Invalid CSV File"
                        return self.form_invalid(form)    
                    obj = UserFile.objects.create(user=request.user, user_file=f, delimiter=delimiter)
                    obj.save()
                else:
                    return PermissionError("File too large")

            return self.form_valid(form)
        else:
            return self.form_invalid(form)

def get_reduced_mass(mass1,mass2):
    mass1 = float(mass1)
    mass2 = float(mass2)
    return (mass1*mass2)/(mass1+mass2)*(1/const.physical_constants.get("electron mass in u")[0])
 
def build_channel_matrix(channels, perturbations):
    output_string = ''
    perturbation_counter = 0
    for i in range(len(channels)):
        tr_string = ''
        for j in range(len(channels)):
            if i==j:
                content = f'<button type="button"  class="col btn btn-lg btn-outline-primary" data-toggle="modal" data-target="#ch-{channels[i].prefix}">Channel {i+1}</button>'
                tr_string += wrap_with_tag('td', content, 'w-50')
            if i>j:
                content = '<button type="button"  class="col btn btn-lg btn-outline-light" data-toggle="modal" data-target="None">.</button>'
                tr_string += wrap_with_tag('td', content, 'w-50')
            if i<j:
                content = f'<button type="button"  class="col btn btn-lg btn-outline-danger" data-toggle="modal" data-target="#p-{ perturbations[perturbation_counter].prefix}">Perturbation {perturbation_counter+1}</button>'
                tr_string += wrap_with_tag('td', content, 'w-50 channel-button')
                perturbation_counter+=1
        output_string += wrap_with_tag('tr', tr_string )
    return wrap_with_tag('table', output_string, attr="table table-bordered")

def wrap_with_tag(tag, content, attr=""):
    return f'<{tag} class="{attr}">{content}</{tag}>' 
    