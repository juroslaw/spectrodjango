from django import template

register = template.Library()

@register.simple_tag(name='keyvalue_2')
def keyvalue_2(dict, key, key2):
    if key2 in dict[key].keys():
        return dict[key][key2]
    else:
         return ""

@register.simple_tag(name='keyvalue')
def keyvalue(dict, key):
    return dict[key]

@register.simple_tag(name='get_form_fields')
def get_form_fields(form, number):
    output_list = []
    for key in form.fields:
        if f'channel-{number}' in key:       
            output_list.append(form[key])
    return output_list