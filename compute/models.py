from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse 
import os


class UserFile(models.Model):
    delimiter_choices = (
        ('\t', 'tab'),
        (',','comma'),
        (';','semi-colon'),
        ('^','caret'),
        ('|','pipe'),
    )
    user = models.ForeignKey(User, related_name='files', on_delete=models.CASCADE)
    user_file = models.FileField(upload_to='uploads/userfiles/')
    delimiter = models.CharField(max_length=2, default="\t", choices=delimiter_choices)
    timestamp = models.DateTimeField(auto_now=True)

    def get_absolute_url(self):
        return reverse('compute:file_detail', args=[self.id])


class GlobalParameters(models.Model):
    reduced_mass = models.FloatField()
    x_min = models.FloatField(default=0.0)
    x_max = models.FloatField(default=1.0)
    v_min = models.FloatField(default=1.0)
    v_max = models.FloatField(default=1.0)
    J_min = models.IntegerField(default=0)
    J_max = models.IntegerField(default=0)
    n_DVR = models.IntegerField(default=300)
    n_states = models.IntegerField(default=99)

class Configuration(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    globalParameters = models.OneToOneField(GlobalParameters, on_delete=models.CASCADE)
    molecule = models.CharField(max_length=20)
    created_at = models.DateTimeField(auto_now_add=True)

    
class Channel(models.Model):
    multiplicity_choices = (
        (1, 'Singlet'),
        (2, 'Doublet'),
        (3, 'Triplet'),
        (4, 'Quartet')        
    )
    lambda_choices = (
        (0, '\u03a3 \u2095'),
        (0, '\u03a3 \u2096'),
        (1, '\u03a0'),
        (2, '\u03b4'),
        (3, '\u03a6')
    )
    parity_choices = (
        ('e', 'e'),
        ('f', 'f')
    )
    omega_choices = (
        ('0+','0+'),
        ('0-','0-'),
        ('1','1'),
        ('2','2'),
        ('3','3'),
    )
    analytical_choices = (
        ('morse', 'Morse'),
        ('lennard-jones', 'Lennard-Jones')
    )
    data_type_choices = (
        ('analytical', 'Analitycal'),
        ('point', 'Point')
    )
    multi = models.IntegerField(choices=multiplicity_choices)
    lambda_number = models.IntegerField(choices=lambda_choices)
    lambda_doubling = models.FloatField()
    parity = models.CharField(choices=parity_choices, max_length=20)
    omega = models.CharField(choices=omega_choices, max_length=20)
    data_type = models.CharField(choices=data_type_choices, max_length=20)
    file = models.ForeignKey(UserFile, default=None, on_delete=models.CASCADE)
    analytical_type = models.CharField(choices=analytical_choices, default='point', max_length=20)
    analytical_D = models.FloatField(default=0.0)
    analytical_a = models.FloatField(default=0.0)
    configuration = models.ForeignKey(Configuration, on_delete=models.CASCADE)

class Perturbation(models.Model):
    a0 = models.DecimalField(default=0.0, decimal_places=20, max_digits=30)
    a1 = models.DecimalField(default=0.0, decimal_places=20, max_digits=30)
    a2 = models.DecimalField(default=0.0, decimal_places=20, max_digits=30)
    a3 = models.DecimalField(default=0.0, decimal_places=20, max_digits=30)
    a4 = models.DecimalField(default=0.0, decimal_places=20, max_digits=30)
    configuration = models.ForeignKey(Configuration, on_delete=models.CASCADE)

class Result(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    configuration = models.ForeignKey(Configuration, null=True, on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=50)

    def get_absolute_url(self):
        return reverse('compute:result_detail', args=[self.id])
    
    def get_zip(self):
        return reverse('compute:download_result', args=[self.id])

class J_Result(models.Model):
    J = models.IntegerField()
    data_file = models.FilePathField(path='/uploads/results/')
    plot_div = models.TextField()
    plot_script = models.TextField()
    Parent = models.ForeignKey(Result, on_delete=models.CASCADE)

    def get_absolute_url(self):
        return reverse('compute:j_result_detail', args=[self.id])

    def delete(self, *args, **kwargs):
        os.remove(self.data_file)
        super(J_Result, self).delete(*args, **kwargs)
