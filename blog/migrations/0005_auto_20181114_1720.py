# Generated by Django 2.1.2 on 2018-11-14 16:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_auto_20181113_1947'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='status',
            field=models.CharField(choices=[('published', 'Opublikowany'), ('draft', 'Roboczy')], default='draft', max_length=10),
        ),
    ]
